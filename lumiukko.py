import turtle
import random


bg = turtle.Screen()
bg.bgcolor("white")


def draw_circle(turtle, color, size, x, y):
  turtle.penup()
  turtle.color(color)
  turtle.fillcolor(color)
  turtle.goto(x,y)
  turtle.pendown()
  turtle.begin_fill()
  turtle.circle(size)
  turtle.end_fill()
  

tommy = turtle.Turtle()
tommy.shape("turtle")
tommy.speed(10)

draw_circle(tommy, "pink", 20, 0, -40)
draw_circle(tommy, "pink", 30, 0, -100)
draw_circle(tommy, "pink", 50, 0, -200)







